-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.25-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for job_recommendation
CREATE DATABASE IF NOT EXISTS `job_recommendation` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `job_recommendation`;


-- Dumping structure for table job_recommendation.education
CREATE TABLE IF NOT EXISTS `education` (
  `education_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `school_name` varchar(255) NOT NULL,
  `school_degree` varchar(50) NOT NULL,
  `start_year` int(4) NOT NULL,
  `end_year` int(4) NOT NULL,
  PRIMARY KEY (`education_id`),
  KEY `FK1_user_education` (`user_id`),
  CONSTRAINT `FK1_user_education` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table job_recommendation.experience
CREATE TABLE IF NOT EXISTS `experience` (
  `experience_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `industry_name` varchar(50) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `experience_months` int(4) NOT NULL,
  `is_current` bit(1) NOT NULL,
  `location_name` mediumtext NOT NULL,
  `job_title` mediumtext NOT NULL,
  PRIMARY KEY (`experience_id`),
  KEY `FK1_user_experience` (`user_id`),
  CONSTRAINT `FK1_user_experience` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table job_recommendation.industry
CREATE TABLE IF NOT EXISTS `industry` (
  `industry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `industry_name` varchar(50) NOT NULL,
  PRIMARY KEY (`industry_id`),
  KEY `FK1_user_industry` (`user_id`),
  CONSTRAINT `FK1_user_industry` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table job_recommendation.location
CREATE TABLE IF NOT EXISTS `location` (
  `location_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `location_name` mediumtext NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `FK1_user_location` (`user_id`),
  CONSTRAINT `FK1_user_location` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table job_recommendation.skill
CREATE TABLE IF NOT EXISTS `skill` (
  `skill_id` int(10) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `skill_name` varchar(50) NOT NULL,
  PRIMARY KEY (`skill_id`),
  KEY `FK1_user_skill` (`user_id`),
  CONSTRAINT `FK1_user_skill` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table job_recommendation.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `headline` varchar(50) NOT NULL,
  `profile_id` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `profile_id` (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
