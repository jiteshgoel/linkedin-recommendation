<?php
  if (!isset($_SESSION['info'])) {
    $li->setAccessToken($_SESSION['li_access_token']);
    $_SESSION['info'] = $li->get('/people/~:(id,first-name,last-name,maiden-name,formatted-name,phonetic-first-name,phonetic-last-name,formatted-phonetic-name,headline,location,industry,current-share,num-connections,num-connections-capped,summary,specialties,positions,picture-url,picture-urls::(original),site-standard-profile-request,api-standard-profile-request,public-profile-url)');
  }
  if (isset($_GET['recommend'])) {
    $li->setAccessToken($_SESSION['li_access_token']);
    $query = '/company-search:(facets,companies:(name,universal-name,website-url,logo-url))?keywords=&industry='.str_replace(' ', '+', $_SESSION['info']['industry']).'&location='.$_SESSION['info']['location']['name'].'&sort=relevance';
    $_SESSION['recommendations'] = $li->get($query);
  }
?>
    <div id="profileHead">
      <div id="profileImg">
        <img src="<?php echo $_SESSION['info']['pictureUrls']['values'][0]; ?>" />
      </div>
      <div id="profileBasic">
        <div>
          <p style="font-weight: bold; font-size: 24px; margin-top: 0px;"><?php echo $_SESSION['info']['formattedName']; ?></p>
          <p style="font-size: 14px; margin-top: -24px;"><?php echo $_SESSION['info']['headline']; ?></p>
          <p style="font-size: 13px; margin-top: -12px; color: #66696a;"><?php echo $_SESSION['info']['location']['name'] . " | " . $_SESSION['info']['industry']; ?></p>
        </div>
        <a href='<?php echo $domain . "/?recommend=1" ?>'><div id="button">Get Job Recommendations</div></a>
        <div style="float: right; font-size: 14px; color: #006fa6; margin-top: 80px; text-align: right;">
          <strong><?php echo $_SESSION['info']['numConnections'];
              if ($_SESSION['info']['numConnectionsCapped'] == 1)
                echo "+"; ?></strong>
          <br />
          <span id="conn">connections</span>
        </div>
      </div>
    </div>
    <?php if (isset($_GET['recommend'])) require ('recommendations.php'); ?>
    <a href="logout.php"><div id="logoutBtn">Logout</div></a>
