<?php
  require_once 'config.php';
  require_once 'LinkedIn.php';

  use LinkedIn\LinkedIn;

  $li = new LinkedIn(
    array(
      'api_key' => '75c2xnmqgteg95',
      'api_secret' => 'DrynyEEdFumUfnVd',
      'callback_url' => $domain
    )
  );

  $url = $li->getLoginUrl(
    array(
      LinkedIn::SCOPE_BASIC_PROFILE,
      LinkedIn::SCOPE_EMAIL_ADDRESS,
    )
  );
?>
<?php require ('header.php'); ?>
<?php
  session_start();
  if (!isset($_SESSION['li_access_token'])) {
    if(isset($_REQUEST['code'])) {
      $token = $li->getAccessToken($_REQUEST['code']);
      $token_expires = $li->getAccessTokenExpiration();
      $_SESSION['li_access_token'] = $token;
      $li->setAccessToken($_SESSION['li_access_token']);
      header('Location: ' . $domain);
    } else {
      require ('signin.php');
    }
  } else {
    require ('home.php');
  }
?>
<?php require ('footer.php'); ?>
