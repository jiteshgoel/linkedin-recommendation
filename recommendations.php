<div id="recommendations">
<?php
for ($i = 0; $i<10; $i++) {
?>
  <div id="job">
    <div id="jobImg">
      <img src="<?php echo $_SESSION['recommendations']['companies']['values'][$i]['logoUrl'] ?>" />
    </div>
    <div id="jobData">
      <p style="font-weight: bold; font-size: 24px; margin-top: 0px;"><?php echo $_SESSION['recommendations']['companies']['values'][$i]['name'] ?></p>
      <p style="font-size: 13px; margin-top: -24px;"><?php echo "<a href=".$_SESSION['recommendations']['companies']['values'][$i]['websiteUrl'].">".$_SESSION['recommendations']['companies']['values'][$i]['websiteUrl']."</a>"; ?></p>
    </div>
  </div>
  <div style="clear:both;"></div>
<?php
}
?>
</div>
